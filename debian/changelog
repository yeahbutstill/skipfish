skipfish (2.10b-2kali5) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Add Uploaders
  * Remove template comment and switch spaces to tabs

  [ Arnaud Rebillout ]
  * Add XS-Debian-Vcs-* fields

  [ Sophie Brun ]
  * Bump Standards-Version to 4.6.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Jun 2022 15:39:27 +0200

skipfish (2.10b-2kali4) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Fix day-of-week for changelog entry 1.13b-1.
  * Drop fields with obsolete URLs.

  [ Arnaud Rebillout ]
  * Configure gbp import-dsc for a Debian-derived package
  * Bump build-dep, libidn11-dev to libidn-dev

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 06 Sep 2021 16:30:09 +0700

skipfish (2.10b-2kali3) kali-dev; urgency=medium

  [ Igor Bezzubchenko ]
  * fixing broken ciphersuite evaluation for newer OpenSSLs

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 08 Jan 2021 11:11:58 +0100

skipfish (2.10b-2kali2) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Ben Wilson ]
  * Consistency with install file

  [ Sophie Brun ]
  * Refresh patch
  * Add a patch to build with openssl 1.1
  * Use debhelper-compat 13
  * Bump Standards-Version to 4.5.1
  * Update debian/watch to version 4
  * Fix short name BSD in debian/copyright
  * Fix small syntax issues in manpage

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Jan 2021 14:58:25 +0100

skipfish (2.10b-2kali1) kali; urgency=low

  * Updated watch file

 -- Mati Aharoni <muts@kali.org>  Sun, 12 Jan 2014 16:16:21 -0500

skipfish (2.10b-2kali0) kali; urgency=low

  * Kali import

 -- Mati Aharoni <muts@kali.org>  Sat, 10 Aug 2013 18:11:12 -0400

skipfish (2.10b-1) unstable; urgency=low

  * The Akamai Technologies paid volunteer days release.
  * New upstream version.
  * Bumped Standards-Version (no changes needed).
  * Various path fixes because of upstream changes.
  * Added new libpcre3-dev build dependency.
  * Totally rewritten copyright file to comply with new copyright standard.

 -- Bartosz Fenski <fenio@debian.org>  Thu, 20 Dec 2012 14:59:36 +0100

skipfish (2.05b-1) unstable; urgency=low

  * New upstream version.

 -- Bartosz Fenski <fenio@debian.org>  Thu, 15 Mar 2012 10:18:34 +0100

skipfish (2.03b-1) unstable; urgency=low

  * New upstream version.

 -- Bartosz Fenski <fenio@debian.org>  Wed, 15 Feb 2012 14:23:24 +0100

skipfish (2.02b-1) unstable; urgency=low

  * New upstream version.

 -- Bartosz Fenski <fenio@debian.org>  Wed, 03 Aug 2011 15:07:13 +0200

skipfish (1.85b-1) unstable; urgency=low

  * New upstream version.
  * Fixed watchfile.
  * Bump Standards-Version (no changes needed).
  * Fixed copyright file to make lintian happy about BSD license.

 -- Bartosz Fenski <fenio@debian.org>  Tue, 29 Mar 2011 17:52:17 +0200

skipfish (1.32b-1) unstable; urgency=low

  * New upstream release.

 -- Bartosz Fenski <fenio@debian.org>  Tue, 20 Apr 2010 19:49:33 +0200

skipfish (1.29b-1) unstable; urgency=low

  * New upstream release.
  * Removed manpage since it's now included by upstream.

 -- Bartosz Fenski <fenio@debian.org>  Mon, 05 Apr 2010 18:43:06 +0200

skipfish (1.26b-1) unstable; urgency=low

  * New upstream release.
  * Skip most dh_* scripts.
  * Includes manpage courtesy of Thorsten Schifferdecker (Closes: #575596)

 -- Bartosz Fenski <fenio@debian.org>  Sat, 27 Mar 2010 10:34:13 +0100

skipfish (1.19b-1) unstable; urgency=low

  * New upstream release.

 -- Bartosz Fenski <fenio@debian.org>  Wed, 24 Mar 2010 08:45:16 +0100

skipfish (1.13b-1) unstable; urgency=low

  * Initial release (Closes: #574756)

 -- Bartosz Fenski <fenio@debian.org>  Mon, 22 Mar 2010 18:41:42 +0100
